<!DOCTYPE html>
<html class="no-js" lang="zxx">

    <head>

        @include('frontend.layouts.layouts-pm.head')

        @yield('css')

    </head>

    <body>
    <!--[if lt IE 8]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--=========================*
             Page Container
    *===========================-->
    <div class="page-container">

        <!--=========================*
                 Side Bar Menu
        *===========================-->
        @include('frontend.layouts.layouts-pm.sidebar')
        <!--=========================*
               End Side Bar Menu
        *===========================-->

        <!--==================================*
                   Main Content Section
        *====================================-->
        <div class="main-content">

            <!--==================================*
                       Header Section
            *====================================-->
            @include('frontend.layouts.layouts-pm.header')
            <!--==================================*
                       End Header Section
            *====================================-->

            <!--==================================*
                       Main Section
            *====================================-->
            <div class="main-content-inner">
                @yield('main-content')
            </div>
            <!--==================================*
                       End Main Section
            *====================================-->

            <div class="text-center">
                @yield('content')
            </div>

        </div>
        <!--=================================*
               End Main Content Section
        *===================================-->

        <!--=================================*
                      Footer Section
        *===================================-->
        @include('frontend.layouts.layouts-pm.footer')
        <!--=================================*
                    End Footer Section
        *===================================-->

    </div>
    <!--=========================*
            End Page Container
    *===========================-->

    <!--=========================*
          Offset Sidebar Menu
    *===========================-->
    @include('frontend.layouts.layouts-pm.offset-menu')
    <!--================================*
             End Offset Sidebar Menu
    *==================================-->




    <!--=========================*
                Scripts
    *===========================-->

    @include('frontend.layouts.layouts-pm.scripts')

    @yield('js')

    </body>
</html>
