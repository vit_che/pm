<?php

Breadcrumbs::for('admin.auth.partner.index', function ($trail) {
    $trail->push(__('labels.backend.access.partners.management'), route('admin.auth.partner.index'));
});

Breadcrumbs::for('admin.auth.partner.create', function ($trail) {
    $trail->parent('admin.auth.partner.index');
    $trail->push(__('labels.backend.access.partners.create'), route('admin.auth.partner.create'));
});

Breadcrumbs::for('admin.auth.partner.edit', function ($trail, $id) {
    $trail->parent('admin.auth.partner.index');
    $trail->push(__('menus.backend.access.partners.edit'), route('admin.auth.partner.edit', $id));
});

