<?php
/**
 * Created by PhpStorm.
 * User: doker
 * Date: 30.10.2019
 * Time: 13:07
 */

namespace App\Http\Requests\Backend\Auth\Partner;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;


class StorePartnerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->isAdmin();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', Rule::unique('partners')],
        ];
    }
}
