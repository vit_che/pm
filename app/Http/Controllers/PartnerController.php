<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partner;
use App\Http\Requests\Backend\Auth\Partner\StorePartnerRequest;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('backend.auth.partner.index')
            ->with('partners', Partner::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.auth.partner.create')
            ->with('partners', Partner::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePartnerRequest $request)
    {
        if ($request->isMethod('post')) {
            $input = $request->except('_token');

            $partner = new Partner();
            $partner->fill($input);
            if ($partner->save()) {

                return redirect()->route('admin.auth.partner.index')->withFlashSuccess(__('alerts.backend.partners.created'));
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        return view('backend.auth.partner.edit')
            ->with('partner', $partner);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        $input = $request->except('_token');
        $partner->fill($input);
        $partner->save();

        return redirect()->route('admin.auth.partner.index')->withFlashSuccess(__('alerts.backend.partners.created'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Partner $partner)
    {
        if ($request->isMethod('DELETE')) {
            if (count($partner->users()->get()) > 0) {

                return redirect()->route('admin.auth.partner.index')->withErrors('The Partner includes users! You must delete them from Partner before');
            }
            $partner->delete();

            return redirect()->route('admin.auth.partner.index')->withFlashSuccess(__('alerts.backend.partners.deleted'));
        }

        return redirect()->route('admin.auth.partner.index')->withFlashError(__('alerts.backend.partners.notdeleted'));
    }

}
