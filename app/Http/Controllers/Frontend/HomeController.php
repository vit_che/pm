<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('frontend.index')->with('user', Auth::user());
    }

    /**
     * @return \Illuminate\View\View
     */
    public function profile()
    {
        return view('frontend.profile')->with('user', Auth::user());
    }
}
